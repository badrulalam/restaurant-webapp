/**
 * MenuController
 *
 * @description :: Server-side logic for managing menus
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
    addMenu: function  (req, res) {
        console.log("entered in addMenu");
        if(req.method === 'GET')
            return res.json({'status':'GET not allowed'});
        // Call to /upload via GET is error
        // var uploadFile = req.file('uploadFile');
        console.log(req.body.userId);
        var uploadFile = req.file('file');

        uploadFile.upload({
            // You can apply a file upload limit (in bytes)
            maxBytes: 1000000,

            //dirname: require('path').resolve(sails.config.appPath, '/assets/images')
            dirname: sails.config.appPath+'/uploads/menu/image/'

        },function onUploadComplete (err, files) {
            // IF ERROR Return and send 500 error with error
            if (err) return res.serverError(err);

            var baseAbsolutePath = process.cwd();
            var imagePath = files[0].fd.replace(baseAbsolutePath,'');

            req.body.image = imagePath;
            //updated to database image path
            Menu.create(req.body).exec(function afterwards(err,data,updated){
                if (err)
                    return res.serverError(err);
                console.log("New Menu created");
                res.json({status:200,data:data});
            });
        });
    }
};

