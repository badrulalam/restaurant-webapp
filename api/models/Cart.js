/**
* Cart.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/
"use strict";

module.exports = {

  attributes: {
    user_id: {
      type: 'string',
      required: true
    } ,
    is_complete: {
      type: 'boolean',
      required: true
    },
    total: {
      type: 'string',
      required : false
    },
    cartitems:{
      collection: 'cartitem',
      via: 'cart'
    }
  }
};

