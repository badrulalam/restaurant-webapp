/**
 * Lineitem.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

    attributes: {
        name: {
            type: 'string',
            required: false
        },
        price: {
            type: 'string',
            required: false
        },
        details: {
            type: 'string',
            required: false
        },
        image: {
            type: 'string',
            required: false
        },
        order: {
            model: 'order'
        }
    }
};

