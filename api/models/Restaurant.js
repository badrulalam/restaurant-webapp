/**
 * Restaurant.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

    attributes: {
        name: {
            type: 'string',
            required: true
        },
        logo: {
            type: 'string',
            required: false
        },
        type: {
            type: 'string',
            required: false
        },
        city: {
            type: 'string',
            required: false
        },
        postcode: {
            type: 'string',
            required: false
        },
        is_enabled : {
          type : 'boolean',
          required: false
        },
        menus: {
            collection: 'menu',
            via: 'restaurant'
        },
        images: {
            collection: 'restuimage',
            via: 'restaurant'
        }
    }
};

