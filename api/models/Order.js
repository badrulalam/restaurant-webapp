module.exports = {

    attributes: {
        number: {
            type: 'string',
            required: false
        },
        total: {
            type: 'string',
            required: false
        },
        details: {
            type: 'string',
            required: false
        },
        image: {
            type: 'string',
            required: false
        },
        restaurant: {
            model: 'restaurant'
        },
        orderitems: {
            collection: 'orderitem',
            via: 'order'
        }

    }
};

