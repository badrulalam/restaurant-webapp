/**
* Cartitem.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    item_id: {
      type: 'string',
      required: false
    },
    item_name: {
      type: 'string',
      required: false
    },
    item_qty: {
      type: 'string',
      required: false
    },
    item_price: {
      type: 'string',
      required: false
    },
    total_price: {
      type: 'string',
      required: false
    },
    cart: {
      model: 'cart'
    }
  }
};
