angular.module('app')
    .controller('NewRestaurantController', function ($scope, $http, $state, $upload) {

        $scope.image_path = false;
        $scope.alerts =[];
        //$scope.uploadFile = function (files) {
        //    var fd = new FormData();
        //    //Take the first selected file
        //    fd.append("file", files[0]);
        //
        //
        //    $http.post('file/upload', fd, {
        //        withCredentials: true,
        //        headers: {'Content-Type': undefined},
        //        transformRequest: angular.identity
        //    }).success(
        //        function (response) {
        //            newfile = response.files;
        //            $scope.restaurant.image = newfile[0].fd.replace(/^.*[\\\/]/, '');
        //        }).error(
        //        function (response) {
        //            alert(response)
        //        }
        //    );

        $scope.restaurant = {};
        $scope.logo = "http://localhost:1337/images/logo.png";
        $scope.images = {"img_path":"http://localhost:1337/images/image1.png", "img_caption": "restaurant image"};
        //$scope.saveRestaurant = function () {
        //
        //    $http.post('/restaurant/create?', $scope.restaurant).success(function (response) {
        //        $http.post('/restuimage/create?restaurant='+ response.id, $scope.images).success(function (response) {
        //            console.log("images uploaded");
        //        });
        //
        //        $state.go('restaurant');
        //    });
        //    //alert(response);
        //};



        //file uploading
        $scope.$watch('files', function () {
            //$scope.upload($scope.files);
        });

        $scope.upload = function (files) {
            $scope.files = files;
            console.log($scope.files);
        };
        $scope.saveRestaurant = function(){
            if ($scope.files && $scope.files.length) {
                for (var i = 0; i < $scope.files.length; i++) {
                    var file = $scope.files[i];
                    console.log("take file");
                    console.log($upload);
                    $upload.upload({
                        url: '/restaurant/addRestaurant',
                        fields: $scope.restaurant,
                        file: file
                    }).progress(function (evt) {
                        //  $("#profile-img-cont").empty().html('<i class="fa fa-refresh fa-spin"></i>');
                        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                        console.log('progress: ' + progressPercentage + '% ' +
                        evt.config.file.name);
                    }).success(function (data, status, headers, config) {
                        console.log('filename: ' + config.file.name + ' uploaded. Response: ' +
                        JSON.stringify(data));
                        $scope.image_path=data.data.post_image;
                        $scope.alerts.push({ type: 'success', msg: 'Well done! You have successfully created a post( '+data.data.post_title+' ).' });
                    });
                }
            }
        }
    });