angular.module('app')
    .controller('MenuController', function ($scope, $http) {

        $http.get('/menu').success(function (response) {
            $scope.menus = response;
        });

        $scope.resfresh = function () {
            $http.get('/menu').success(function (response) {
                $scope.menus = response;
            });
        };


        $scope.delete = function (menu) {
            $http.post('/menu/destroy?id=' + menu.id).success(function (response) {
                console.log("successfull");
            });
        }
    });
/**
 * Created by robin on 4/8/15.
 */
