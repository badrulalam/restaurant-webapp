angular.module('app')
    .controller('RestaurantController', function ($scope, $http) {


        $http.get('/restaurant').success(function (response) {
            $scope.restaurants = response;
        });

        $scope.resfresh = function () {
            $http.get('/restaurant').success(function (response) {
                $scope.restaurants = response;
            });
        };


        $scope.delete = function (res) {
            $http.post('/restaurant/destroy?id=' + res.id).success(function (response) {
                console.log("successfull");
            });
        }
    });