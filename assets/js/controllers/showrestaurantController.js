angular.module('app')
    .controller('ShowRestaurantController', function ($scope, $stateParams, $http, $upload) {
        $scope.image_path = false;
        $scope.alerts =[];

        $http.get('/restaurant/' + $stateParams.id).success(function (response) {
            $scope.res = response;
        });

        $http.get('/menu?restaurant=' + $stateParams.id).success(function (response) {
            $scope.menus = response;
        });

        //$scope.addMenu = function () {
        //    $http.post('menu/create?restaurant='+$stateParams.id, $scope.newMenu).success(function (response) {
        //        $scope.menus.push(response);
        //    });
        //};

        $scope.delete = function (menu) {
            $http.post('/menu/destroy?id=' + menu.id).success(function (response) {
                console.log("successfull");
            });
        };

        //file uploading
        $scope.$watch('files', function () {
            //$scope.upload($scope.files);
        });

        $scope.upload = function (files) {
            $scope.files = files;
            console.log($scope.files);
        };
        $scope.saveMenu = function(){
            if ($scope.files && $scope.files.length) {
                for (var i = 0; i < $scope.files.length; i++) {
                    var file = $scope.files[i];
                    console.log("take file");
                    console.log($upload);
                    $upload.upload({
                        url: '/menu/addMenu',
                        fields: $scope.newMenu,
                        file: file
                    }).progress(function (evt) {
                        //  $("#profile-img-cont").empty().html('<i class="fa fa-refresh fa-spin"></i>');
                        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                        console.log('progress: ' + progressPercentage + '% ' +
                        evt.config.file.name);
                    }).success(function (data, status, headers, config) {
                        console.log('filename: ' + config.file.name + ' uploaded. Response: ' +
                        JSON.stringify(data));
                        $scope.image_path=data.image;
                        //$scope.alerts.push({ type: 'success', msg: 'Well done! You have successfully created a post( '+data.data.post_title+' ).' });

                            $http.post('/menu/update/'+data.data.id+'?restaurant='+$stateParams.id).success(function (response) {
                                $scope.menus.push(response);
                                console.log('restaurant ref added');
                            });
                    });
                }
            }
        }
    });