/**
 * Created by robin on 4/8/15.
 */
angular.module('app')
    .controller('ShowMenuController', function ($scope, $stateParams, $http) {


        $http.get('/menu/' + $stateParams.id).success(function (response) {

            $scope.menu = response;
        });

        $http.get('/item?menu=' + $stateParams.id).success(function (response) {
            $scope.items = response;
        });

        $scope.addItem = function () {
            //$scope.newitem.menu = $stateParams.id;
            $http.post('item/create?menu='+$stateParams.id, $scope.newItem).success(function (response) {
                $scope.items.push(response);
            });
        };


        $scope.delete = function (item) {
            $http.post('/item/destroy?id=' + item.id).success(function (response) {
                console.log("successfull");
            });
        }
    });