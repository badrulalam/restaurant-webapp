angular.module('app')
    .config(function ($stateProvider, $urlRouterProvider, AccessLevels) {

        $stateProvider
            .state('anon', {
                abstract: true,
                template: '<ui-view/>',
                data: {
                    access: AccessLevels.anon
                }
            })
            .state('anon.home', {
                url: '/',
                templateUrl: 'home.html'
            })
            .state('anon.login', {
                url: '/login',
                templateUrl: 'auth/login.html',
                controller: 'LoginController'
            })

            .state('restaurant', {
                url: '/restaurant',
                templateUrl: 'restaurant.html',
                controller: 'RestaurantController',
                data: {
                    access: AccessLevels.user
                }
            })
            .state('restaurant.newrestaurant', {
                url: '/new',
                templateUrl: 'newrestaurant.html',
                controller: 'NewRestaurantController'
            })

            .state('restaurant.showrestaurant', {
                url: '/show/:id',
                templateUrl: 'showrestaurant.html',
                controller: 'ShowRestaurantController'
            })
            .state('menu',{
                url:'/menu',
                templateUrl: 'menu.html',
                controller: 'MenuController',
                data: {
                    access: AccessLevels.user
                }
            })
            .state('menu.showmenu', {
                url: '/show/:id/',
                templateUrl: 'showmenu.html',
                controller: 'ShowMenuController'
            })


            .state('anon.register', {
                url: '/register',
                templateUrl: 'auth/register.html',
                controller: 'RegisterController'
            });

        $stateProvider
            .state('user', {
                abstract: true,
                template: '<ui-view/>',
                data: {
                    access: AccessLevels.user
                }
            })
            .state('user.messages', {
                url: '/messages',
                templateUrl: 'user/messages.html',
                controller: 'MessagesController'
            });

        //$urlRouterProvider.otherwise('/');
    });